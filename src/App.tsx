import React from 'react';
import Routes from './routes/index';
import { BrowserRouter } from 'react-router-dom';
import 'antd/dist/antd.css';
import './theme.less';

import { AppContextProvider } from './context';

function App() {
  return (
    <div className='App'>
      <AppContextProvider>
        <BrowserRouter>
          <Routes />
        </BrowserRouter>
      </AppContextProvider>
    </div>
  );
}

export default App;
