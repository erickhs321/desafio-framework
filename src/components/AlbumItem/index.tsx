import React from 'react';
import './style.css';
import IAlbum from '../../interfaces/IAlbum';
import { Avatar } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import FileImageOutlined from '@ant-design/icons/lib/icons/FileImageOutlined';

const AlbumItem: React.FC<IAlbum> = (props) => {
  return (
    <div className='album-item__container' style={props.style} key={props.id}>
      <div className='album-item__header'>
        <div>
          <Avatar size={48} icon={<UserOutlined />} /> <span>User Name</span>
        </div>
        <div>
          <h3>{props.title}</h3>
        </div>
      </div>
      <div>
        <Avatar size={100} icon={<FileImageOutlined />} />
      </div>
    </div>
  );
};
export default AlbumItem;
