import React, { ReactNode } from 'react';

import './style.css';
type Props = {
  children: ReactNode;
};
const Container: React.FC<Props> = (props) => {
  return <div className='container'>{props.children}</div>;
};

export default Container;
