import React from 'react';
import { Avatar } from 'antd';
import './style.css';
import IPost from '../../interfaces/IPost';
import UserOutlined from '@ant-design/icons/lib/icons/UserOutlined';

const PostItem: React.FC<IPost> = (props) => {
  return (
    <div className='post-item__container' style={props.style} key={props.id}>
      <div>
        <div className='post-item__header'>
          <div>
            <Avatar size={48} icon={<UserOutlined />} /> <span>User Name</span>
          </div>
          <div>
            <h3> {props.title}</h3>
          </div>
        </div>
        <p>{props.body}</p>
        <div className='post-item__comments'></div>
      </div>
    </div>
  );
};
export default PostItem;
