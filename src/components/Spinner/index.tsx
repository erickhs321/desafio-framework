import { Spin, Alert } from 'antd';

import React from 'react';
import './style.css';

const Spinner: React.FC = () => {
  return (
    <Spin tip='Carregando...' size='large'>
      <Alert
        message='Alert message title'
        description='Further details about the context of this alert.'
        type='info'
      />
    </Spin>
  );
};

export default Spinner;
