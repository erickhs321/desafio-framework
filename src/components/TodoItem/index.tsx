import React from 'react';
import './style.css';
import ITodo from '../../interfaces/ITodo';
import { Checkbox } from 'antd';

const TodoItem: React.FC<ITodo> = (props) => {
  return (
    <div style={props.style} className='todo-item__container'>
      <Checkbox checked={props.completed}>{props.title}</Checkbox>
    </div>
  );
};
export default TodoItem;
