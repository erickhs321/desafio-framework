import React from 'react';
import { Link } from 'react-router-dom';
import { ReactComponent as Logo } from '../../assets/img/logo.svg';
import { Layout, Menu } from 'antd';

import './style.css';

const Topbar: React.FC = () => {
  const { Header } = Layout;
  return (
    <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
      <div className='logo'>
        <Logo />
      </div>
      <Menu theme='dark' mode='horizontal' defaultSelectedKeys={['1']}>
        <Menu.Item key='1'>
          <Link to='/'>
            <i className='fas fa-user-plus'></i>
            <span>Posts</span>
          </Link>
        </Menu.Item>
        <Menu.Item key='2'>
          <Link to='/todos'>
            <i className='fas fa-users'></i>
            <span>Tarefas</span>
          </Link>
        </Menu.Item>

        <Menu.Item key='3'>
          <Link to='/albums'>
            <i className='fas fa-user-plus'></i>
            <span>Album</span>
          </Link>
        </Menu.Item>
      </Menu>
    </Header>
  );
};
export default Topbar;
