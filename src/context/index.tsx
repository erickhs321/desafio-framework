import { createContext, useState, ReactNode, useContext } from 'react';
import IAlbum from '../interfaces/IAlbum';
import IComment from '../interfaces/IComment';
import IPhoto from '../interfaces/IPhoto';
import IPost from '../interfaces/IPost';
import ITodo from '../interfaces/ITodo';
import api from '../services/api';

interface IStore {
  posts?: IPost[];
  getPosts?: () => void;
  todos?: ITodo[];
  getTodos?: () => void;
  albums?: IAlbum[];
  getAlbums?: () => void;
  photos?: IPhoto[];
  comments?: IComment[];
  loading?: boolean;
}

const AppContext = createContext<IStore>({});

type Props = {
  children: ReactNode;
};

export function AppContextProvider(props: Props) {
  const { children } = props;
  const [posts, setPosts] = useState<IPost[]>([]);
  const [todos, setTodos] = useState<ITodo[]>([]);
  const [albums, setAlbums] = useState<IAlbum[]>([]);
  const [photos, setPhotos] = useState<IPhoto[]>([]);
  const [comments, setComments] = useState<IComment[]>([]);
  const [loading, setLoading] = useState(true);

  const getPosts = () => {
    api
      .get<IPost[]>('/posts')
      .then((res) => {
        setPosts(res.data);
      })
      .finally(() => setLoading(false));
  };

  const getTodos = () => {
    api
      .get<ITodo[]>('/todos')
      .then((res) => setTodos(res.data))
      .finally(() => setLoading(false));
  };

  const getAlbums = () => {
    api
      .get<ITodo[]>('/albums')
      .then((res) => setAlbums(res.data))
      .finally(() => setLoading(false));
  };

  const value = {
    posts,
    getPosts,
    todos,
    getTodos,
    albums,
    getAlbums,
    photos,
    comments,
    loading,
  };

  return <AppContext.Provider value={value}>{children}</AppContext.Provider>;
}

export function useAppContext() {
  const context = useContext(AppContext);

  return context;
}
