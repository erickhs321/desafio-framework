export default interface IAlbum {
  userId: number;
  id: number;
  title: string;
  style?: React.CSSProperties;
}
