import IComment from './IComment';

export default interface IPost {
  userId: number;
  id: number;
  title: string;
  body: string;
  comments?: IComment[] | IComment;
  style?: React.CSSProperties;
}
