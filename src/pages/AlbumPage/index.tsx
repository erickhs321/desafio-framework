import React, { useEffect } from 'react';
import { useAppContext } from '../../context';
import { FixedSizeList } from 'react-window';
import AutoSizer from 'react-virtualized-auto-sizer';
import './style.css';
import AlbumItem from '../../components/AlbumItem';
import Spinner from '../../components/Spinner';

const AlbumPage: React.FC = () => {
  const { albums, getAlbums, loading } = useAppContext();

  useEffect(() => {
    getAlbums && getAlbums();
  }, []);

  const Row = ({
    index,
    style,
  }: {
    index: number;
    style: React.CSSProperties;
  }) => {
    const item = albums && albums[index];

    return (
      <AlbumItem
        style={{ ...style }}
        userId={item?.userId || 0}
        id={item?.id || 0}
        key={item?.id}
        title={item?.title || ''}
      />
    );
  };

  return (
    <div className='todo-page__container'>
      {loading && <Spinner />}
      {!loading && (
        <AutoSizer>
          {({ height, width }) => (
            <FixedSizeList
              width={width}
              height={height}
              itemCount={albums?.length || 0}
              itemSize={300}
              layout={'vertical'}
            >
              {Row}
            </FixedSizeList>
          )}
        </AutoSizer>
      )}
    </div>
  );
};
export default AlbumPage;
