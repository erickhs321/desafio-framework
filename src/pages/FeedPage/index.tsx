import React, { useEffect } from 'react';
import { useAppContext } from '../../context';
import { FixedSizeList } from 'react-window';
import AutoSizer from 'react-virtualized-auto-sizer';
import './style.css';
import PostItem from '../../components/PostItem';
import Spinner from '../../components/Spinner';

const FeedPage: React.FC = () => {
  const { posts, getPosts, loading } = useAppContext();

  useEffect(() => {
    getPosts && getPosts();
  }, []);

  const Row = ({
    index,
    style,
  }: {
    index: number;
    style: React.CSSProperties;
  }) => {
    const item = posts && posts[index];

    return (
      <PostItem
        style={{ ...style }}
        body={item?.body || ''}
        id={item?.id || 0}
        userId={item?.userId || 0}
        title={item?.title || ''}
      />
    );
  };

  return (
    <div className='feedback-page__container'>
      {loading && <Spinner />}
      {!loading && (
        <AutoSizer>
          {({ height, width }) => (
            <FixedSizeList
              width={width}
              height={height}
              itemCount={posts?.length || 0}
              itemSize={190}
            >
              {Row}
            </FixedSizeList>
          )}
        </AutoSizer>
      )}
    </div>
  );
};
export default FeedPage;
