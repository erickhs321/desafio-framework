import React, { useEffect } from 'react';
import { useAppContext } from '../../context';
import { FixedSizeList } from 'react-window';
import AutoSizer from 'react-virtualized-auto-sizer';
import './style.css';
import TodoItem from '../../components/TodoItem';
import Spinner from '../../components/Spinner';

const TodoPage: React.FC = () => {
  const { todos, getTodos, loading } = useAppContext();

  useEffect(() => {
    getTodos && getTodos();
  }, []);

  const Row = ({
    index,
    style,
  }: {
    index: number;
    style: React.CSSProperties;
  }) => {
    const item = todos && todos[index];

    return (
      <TodoItem
        style={{ ...style }}
        userId={item?.userId || 0}
        id={item?.id || 0}
        key={item?.id}
        completed={item?.completed || false}
        title={item?.title || ''}
      />
    );
  };

  return (
    <div className='todo-page__container'>
      {loading && <Spinner />}
      {!loading && (
        <AutoSizer>
          {({ height, width }) => (
            <FixedSizeList
              width={width}
              height={height}
              itemCount={todos?.length || 0}
              itemSize={50}
            >
              {Row}
            </FixedSizeList>
          )}
        </AutoSizer>
      )}
    </div>
  );
};
export default TodoPage;
