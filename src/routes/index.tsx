import { Route, Switch } from 'react-router-dom';
import Topbar from '../components/Topbar';
import FeedPage from '../pages/FeedPage';
import TodoPage from '../pages/TodoPage';
import AlbumPage from '../pages/AlbumPage';
import Container from '../components/Container';

const Routes = () => {
  return (
    <Switch>
      <Route exact path='/'>
        <Topbar />
        <Container>
          <FeedPage />
        </Container>
      </Route>

      <Route exact path='/todos'>
        <Topbar />
        <Container>
          <TodoPage />
        </Container>
      </Route>

      <Route path='/albums'>
        <Topbar />
        <Container>
          <AlbumPage />
        </Container>
      </Route>
    </Switch>
  );
};

export default Routes;
